# GUINEA-PIG

## 1. Installation

### To compile without FFTW libraries:  

    ./configure -prefix=$YOUR_INSTALL_DIR  
    make  
    make install

### To compile with FFTW libraries:


#### FFTW Libraries version 2.X.X:

    ./configure -prefix=$YOUR_INSTALL_DIR -enable-fftw2 -with-fftwdir=$YOUR_FFTW_HOME
    make 
    make install

#### FFTW Libraries version 3.X.X:

    ./configure -prefix=$YOUR_INSTALL_DIR -enable-fftw3 -with-fftwdir=$YOUR_FFTW_HOME
    make
    make install


The "guinea" executable will be in the $YOUR_INSTALL_DIR/bin directory.

## 2. Run GuineaPig

### To run a single bunch crossing:  

    ./bin/guinea  --acc_file testing/acc_C3_v2.dat  C3 C3_comp_pars output/output_test.ref  

Move the output files, e.g. pairs.dat which contains information for pair-produced electrons and positrons,in the **output** folder.  
You can produce histograms in ROOT using the read_pairs.C script in the **scripts** folder.

### To run multiple bunch crossings:    

1. Make sure you initialize rndm_seed to 1 in testing/acc_C3_v2.dat before running.
2. Run the shell script in the **scripts** folder:  

        ./GP_multiple_runs.sh  

3. Produce histograms using the read_pairs_multiple_runs.C script in the **scripts** folder.


