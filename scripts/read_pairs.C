
#include "TH1D.h"
#include "TH1F.h"
#include "TH2D.h"
#include "TH2F.h"
#include "TTree.h"
#include "TFile.h"
#include "TDirectory.h"
#include "TPaveText.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TStyle.h"
#include "TChain.h"
#include <iostream>
#include <fstream>
#include "TSystem.h"
#include "TROOT.h"


std::ofstream outputfile;


double *  pair_info(double energy, double beta_x, double beta_y, double  beta_z, double  vtx_x, double vtx_y, double vtx_z);





void read_pairs()

{



std::ifstream infile("../output/pairs.dat");


std::string line;


TH1D *h_energy = new TH1D("h_energy","h_energy",100,-15,15);
TH1D *h_pos = new TH1D("h_pos","h_pos",2,0,2);
TH1D *h_x = new TH1D("h_x","h_x",100,-10000,10000);
TH1D *h_y = new TH1D("h_y","h_y",100,-10000,10000);
TH1D *h_z = new TH1D("h_z","h_z",100,-800000,800000);
TH1D *h_mass = new TH1D("h_mass","h_mass",200,0.00045,0.00060);
TH1D *h_boost = new TH1D("h_boost","h_boost",100,-2,2);
TH1D *h_beta_x = new TH1D("h_beta_x","h_beta_x",200,-1.1,1.1);
TH1D *h_beta_y = new TH1D("h_beta_y","h_beta_y",200,-1.1,1.1);
TH1D *h_beta_z = new TH1D("h_beta_z","h_beta_z",200,-1.1,1.1);
TH1D *h_energies_ratio = new TH1D("h_energies_ratio","h_energies_ratio",200,0.992,1.008);


while (std::getline(infile, line))
{
    std::istringstream iss(line);
    double a, b,c,d,e,f,g,h,i,j;
    if (!(iss >> a >> b >> c >> d >> e >> f >> g >> h >> i >> j)) { break; } // error

    

    double *r;
    r =	pair_info(a,b,c,d,e,f,g);


    double pos = r[0];
	double energy=r[1];
	double x = r[2];
	double y = r[3];
	double z = r[4];
	double mass = r[5];
	double boost = r[6];
	double beta_x = r[7];
	double beta_y = r[8];
	double beta_z = r[9];
    double energy_from_momenta = r[10];
	
	cout << " pos = " << pos << " energy = " << energy << " E from momenta =" << energy_from_momenta << " x = "<< x << "  y = "<< y << " z =  " << z << " mass =  " << mass << "  boost = " << boost << " beta x =" << beta_x << " beta y = " << beta_y << "  beta z = " << beta_z << endl;
	
	
    h_energy -> Fill(energy);
    h_pos -> Fill(pos);
    h_x -> Fill(x);
    h_y -> Fill(y);
    h_z -> Fill(z);
    if (mass>0. && mass<=1.) h_mass -> Fill(mass);
    h_boost -> Fill(boost);
    h_beta_x -> Fill(beta_x);
    h_beta_y -> Fill(beta_y);
    h_beta_z -> Fill(beta_z);
    h_energies_ratio -> Fill(energy/energy_from_momenta);
	
}

double entries = h_pos->GetEntries();

for(int i=0;i<=h_pos->GetNbinsX();i++)
{
	double bincontent = h_pos->GetBinContent(i); 
	
	h_pos -> SetBinContent(i,bincontent/entries);
}



TCanvas *c1 = new TCanvas("c1","c1",1000,500);
c1 -> cd();
h_energy -> SetTitle("Particle Energy; Energy (GeV) ; Entries");
h_energy -> Draw("hist");
c1 -> Draw();


TCanvas *c2 = new TCanvas("c2","c2",1000,500);
c2 -> cd();
h_pos -> SetTitle("Electron/Positron Flag; e^{-}/e^{+} flag ; Fraction of Entries");
h_pos -> GetYaxis() -> SetRangeUser(0,1);
h_pos -> GetXaxis() -> SetRangeUser(0.,2);
h_pos -> Draw("hist");
c2 -> Draw();

TCanvas *c3 = new TCanvas("c3","c3",1000,500);
c3 -> cd();
h_x -> SetTitle("Particle x-position;x (nm) ; Entries");
h_x -> Draw("hist");
c3 -> Draw();

TCanvas *c4 = new TCanvas("c4","c4",1000,500);
c4 -> cd();
h_y -> SetTitle("Particle y-position;y (nm) ; Entries");
h_y -> Draw("hist");
c4 -> Draw();

TCanvas *c5 = new TCanvas("c5","c5",1000,500);
c5 -> cd();
h_z -> SetTitle("Particle z-position;z (nm) ; Entries");
h_z -> Draw("hist");
c5 -> Draw();

TCanvas *c6 = new TCanvas("c6","c6",1000,500);
c6 -> cd();
h_mass -> SetTitle("Particle Mass; Mass (GeV/c^{2}) ; Entries");
h_mass -> Draw("hist");
c6 -> Draw();

TCanvas *c7 = new TCanvas("c7","c7",1000,500);
c7 -> cd();
h_boost -> SetTitle("Particle Boost; p_{T}/p_{z} ; Entries");
h_boost -> Draw("hist");
c7 -> Draw();

TCanvas *c8 = new TCanvas("c8","c8",1000,500);
c8 -> cd();
h_beta_x -> SetTitle("Velocity in x-direction; #beta_{x} ; Entries");
h_beta_x -> Draw("hist");
c8 -> Draw();

TCanvas *c9 = new TCanvas("c9","c9",1000,500);
c9 -> cd();
h_beta_y -> SetTitle("Velocity in y-direction;#beta_{y} ; Entries");
h_beta_y -> Draw("hist");
c9 -> Draw();

TCanvas *c10 = new TCanvas("c10","c10",1000,500);
c10 -> cd();
h_beta_z -> SetTitle("Velocity in z-direction; #beta_{z} ; Entries");
h_beta_z -> Draw("hist");
c10 -> Draw();

TCanvas *c11 = new TCanvas("c11","c11",1000,500);
c11 -> cd();
h_energies_ratio -> SetTitle("Energy ratio; #frac{Energy_{GP}}{Energy_{calc}} ; Entries");
h_energies_ratio -> Draw("hist");
c11 -> SetLogy(1);
c11 -> Draw();


TFile *fout = new TFile("../output/GP_pair_histos.root","RECREATE");
fout -> cd();

h_energy->Write();
h_pos->Write();
h_x->Write();
h_y->Write();
h_z->Write();
h_mass->Write();
h_boost->Write();
h_beta_x-> Write();
h_beta_y-> Write();
h_beta_z-> Write();
h_energies_ratio -> Write();


fout -> Close();





}








// https://www.tutorialspoint.com/cplusplus/cpp_return_arrays_from_functions.htm

double * pair_info(double energy, double beta_x, double beta_y, double  beta_z, double  vtx_x, double vtx_y, double vtx_z)

{

const double electron_mass =  0.00051099895 ;  // electron mass in GeV/c^2: From PDG:  https://pdg.lbl.gov/2022/reviews/contents_sports.html

int pos = 0;
double beta = 0.;
double gamma = 0.;
double mass = 0.;
double beta_t = 0.;
double boost = 0.;



if (energy<0)
{	
pos=1;
//energy=-energy;
}

beta = sqrt(beta_x*beta_x+beta_y*beta_y+beta_z*beta_z);

if (beta >=1)
   {
	cout << "=== Caution: Beta>=1 found! ======= " << endl;
	gamma = 1e20;
   }
else
	gamma = 1./sqrt(1-beta*beta);

//gamma = 1./sqrt(1-beta*beta);

mass = energy/(gamma); // in GeV/c^2
if(mass<0) mass=-mass;

beta_t = sqrt(beta_x*beta_x+beta_y*beta_y);
boost = beta_t/beta_z;



double p_x = gamma*electron_mass*beta_x; // units: GeV/c
double p_y = gamma*electron_mass*beta_y; // units: GeV/c
double p_z = gamma*electron_mass*beta_z; // units: GeV/c


double p = sqrt(p_x*p_x+p_y*p_y+p_z*p_z) ; // units: GeV/c

double energy_from_momenta = sqrt(p*p+electron_mass*electron_mass);

if(pos==1) energy_from_momenta=-energy_from_momenta;


static double  r[11];

r[0]=pos;
r[1]=energy;
r[2]=vtx_x;
r[3]=vtx_y;
r[4]=vtx_z;
r[5]=mass;
r[6]=boost;
r[7]=beta_x;
r[8]=beta_y;
r[9]=beta_z;
r[10]=energy_from_momenta;


   return r; // (el/pos,E,x,y,z,mass,boost)

}

























